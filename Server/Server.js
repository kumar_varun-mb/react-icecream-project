const express = require('express');
const app = express();
const cors = require('cors');
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');

app.use(cors());

app.use(bodyParser.json());

app.get('/products', (req, res) => {
    fs.readFile(path.join(__dirname, 'data/products.json'), 'utf-8', (err, data) => {
        if (err)
            console.log(err);
        else {
            res.end(data);
        }
    })
    // let data = fs.readFileSync(path.join(__dirname, 'data/products.json'))
    // res.end(data);
});

app.post('/products/post', (req, res) => {
    if (!req)
        console.log('req doesnt exist');
    else {
        console.log(req.body);
        fs.writeFile(path.join(__dirname, 'data/products.json'), JSON.stringify(req.body,null,'\t'), 'utf-8', (err) => {
            if (err)
                console.log(err);
        })
        // fs.writeFileSync(path.join(__dirname, 'data/products.json'), JSON.stringify(req.body, null, '\t'), 'utf-8');
    }
});

app.get('/orders', (req, res) => {
    fs.readFile(path.join(__dirname, 'data/orders.json'), 'utf-8', (err, data) => {
        if (err)
            console.log(err);
        else {
            res.end(data);
        }
    })
    // let data = fs.readFileSync(path.join(__dirname, 'data/orders.json'))
    // res.end(data);
});

app.post('/orders/post', (req, res) => {
    if (!req)
        console.log('req doesnt exist');
    else {
        console.log(req.body);
        fs.writeFile(path.join(__dirname, 'data/orders.json'), JSON.stringify(req.body,null,'\t'), 'utf-8', (err) => {
            if (err)
                console.log(err);
        })
        // fs.writeFileSync(path.join(__dirname, 'data/orders.json'), JSON.stringify(req.body, null, '\t'), 'utf-8');
    }
});

app.listen(8000, () => {
    console.log("Server Running at port 8000");
});