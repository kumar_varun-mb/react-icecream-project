import React from 'react'
import { Link } from 'react-router-dom'

function Navbar() {
    return (
        <nav className='navbar'>
            <Link to='/'>
                <div className='logo'>
                    <img src={'https://icons-for-free.com/download-icon-line+version+svg+ice+cream-1319964488959330493_512.png'} />
                    IceCreamz
                </div>
            </Link>
            <div className='navbar-items'><Link to='/cart'>Go To Cart</Link></div>
            <div className='navbar-items'><Link to='/orders'>Recent Orders</Link></div>
        </nav>
    )
}
export default Navbar