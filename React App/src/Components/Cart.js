import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ProductContainer from './ProductContainer'
import { ProductContext } from './ProductContext'

class Cart extends Component {
  render() {
    return (
      <div className='cart'>
        <ProductContext.Consumer>
          {
            (productContextProps) => {
              return (
                <>
                  <div className='cart-products'>
                    {Object.entries(productContextProps.productList).map((product) => {
                      if (product[1].qty > 0) {
                        return <ProductContainer product={product} {...productContextProps} />
                      }
                      else
                        return null
                    })}
                  </div>
                  {
                    productContextProps.totalAmount > 0
                      ?
                      <div className='order-details-bar'>
                        <div className='order-total'>
                          Total: Rs.{productContextProps.totalAmount} /-
                        </div>
                        <Link to='/orders'><button className='place-order-btn' onClick={productContextProps.placeOrder}>Place Order</button></Link>
                      </div>

                      :
                      <div className='empty-cart'>
                        <img src='https://sethisbakery.com/assets/website/images/empty-cart.png'></img>
                      </div>
                  }
                </>
              )
            }
          }
        </ProductContext.Consumer>
      </div>
    )
  }
}

export default Cart



