import React from 'react'
import {useParams} from 'react-router-dom'

function OrderDetails(props) {
    const { id } = useParams();
    const orderData = props.orderList[id];
  return (
    <div className='orderdetailsbox'>
              <h1>Order No.{id}</h1>
              <br/><br/>
              <div>
                <div><h2>Item Name</h2></div>
                <div><h2>Qty</h2></div>
              </div>
              <br />
              <br />
              {(orderData)
              ?
              <>
              {orderData.orderList.map((item) => {
                return (
                  <>
                    <div key={Math.random()}>
                      <div><h2>{item.name}</h2></div>
                      <div><h2>{item.qty}</h2></div>
                    </div>
                    <br />
                  </>
                )
              })}
              
            <br />
            <br />
             <h1>Total: Rs.{orderData.totalAmount}.00 /-</h1>
             </>
              :
              null
              }
              
            </div>
  )
}

export default OrderDetails