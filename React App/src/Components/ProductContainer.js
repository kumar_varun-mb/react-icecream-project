import React, { Component } from 'react'
import ProductDetails from './ProductDetails'
import { Routes, Route } from 'react-router-dom'
import { Link } from 'react-router-dom'

class ProductContainer extends Component {
    constructor(props) {
        super(props)
    }


    render() {
        return (
            <div className='product-container'>
                <div className='product-image'>
                    <Link to={`/${this.props.product[0]}`}> <img src={this.props.product[1].imgLink}></img></Link>
                </div>
                <Link to={`/${this.props.product[0]}`}><h3>{this.props.product[1].name}</h3></Link>
                <h3>Rs. {this.props.product[1].price}.00 /-</h3>
                {
                    (this.props.product[1].qty == 0)
                        ?
                        <button className='addToCartBtn' onClick={() => this.props.increment(this.props.product[0])}>Add To Cart</button>
                        :
                        <div className='quantity-controller'>
                            <div className='inner' id="minus" onClick={() => this.props.decrement(this.props.product[0])}>
                                -
                            </div>
                            <div className='inner-qty'>
                                <p>{this.props.product[1].qty}</p>
                            </div>
                            <div className='inner' id="plus" onClick={() => this.props.increment(this.props.product[0])}>
                                +
                            </div>
                        </div>
                }
            </div>
        )
    }
}

export default ProductContainer