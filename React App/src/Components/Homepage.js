import React, { Component } from 'react'
import ProductContainer from './ProductContainer'
import { ProductContext } from './ProductContext'
import ProductDetails from './ProductDetails'
import { Routes, Route } from 'react-router-dom'

class Homepage extends Component {

  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <div className='homepage'>
        <br />
        <h1>Available Items</h1>
        <div className="productList-container">
          <ProductContext.Consumer>
            {
              (productContextProps) => {
                return Object.entries(productContextProps.productList).map((product) => {
                  return <ProductContainer product={product} {...productContextProps} />
                })
              }
            }
          </ProductContext.Consumer>
        </div>
      </div>
    )
  }
}

export default Homepage