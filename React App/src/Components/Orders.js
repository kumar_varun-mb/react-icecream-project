import { ProductContext } from './ProductContext'

import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export class Orders extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  
  componentDidMount() {
      fetch('http://localhost:8000/orders')
        .then(res => {
          return res.json()
        })
        .then(orders => {
          this.setState(orders)
        })
        console.log('reloading')
  }
  
  render() {
    return (
      <div className='orders'>
        <br />
        <br />
        <h1>Past Orders:</h1>
        <br />
       
        {Object.entries(this.state).map((order) => {
          let orderId = order[0];
          let totalAmount = this.state[orderId]['totalAmount'];
          let itemList = this.state[orderId]['orderList'];
          return (
            <div className='orderbox' key={orderId}>
              <Link to = {`${orderId}`}><h2>Order No.{orderId}</h2></Link>
              <br/>
              <div>
                <div>Item Name</div>
                <div>Qty</div>
              </div>
              <br />
              {
                itemList.map((item) => {
                  return (
                    <>
                      <div key={Math.random()}>
                        <div>{item.name}</div>
                        <div>{item.qty}</div>
                      </div>
                      <br />
                    </>
                  )
                })
              }
              <br />
              <br />
              <h2>Total: Rs.{totalAmount}.00 /-</h2>
              
            </div>
          )
        })
        }

      </div>
    )
  }
}

export default Orders