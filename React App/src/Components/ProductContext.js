import React from "react";

const ProductContext = React.createContext({
    productList: {},
    totalAmount: 0,
    increment: () => { },
    decrement: () => { },
    placeOrder: () => { }
});

class CartOperations extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            code: 0,
            response: {
                data: [],
                totalAmount: 0
            }
        }
    }

    totalAmount = 0;
    componentDidMount() {
        fetch('http://localhost:8000/products')
            .then(res => {
                return res.json()
            })
            .then(obj => {
                this.setState(obj)
            })
        this.totalAmount = this.state.response.totalAmount;
    }


    increment = (productID) => {
        let obj = this.state;
        let productList = this.state.response.data;
        productList[productID]['qty'] += 1;
        this.totalAmount += productList[productID]['price'];
        obj['response']['data'] = productList;
        obj['response']['totalAmount'] = this.totalAmount;
        this.setState(obj);

        fetch('http://localhost:8000/products/post', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state, null, '\t')
        })
    }

    decrement = (productID) => {
        let obj = this.state;
        let productList = this.state.response.data;
        (productList[productID]['qty'] > 0) ? (productList[productID]['qty'] -= 1) : (productList[productID]['qty'] = 0)
        (this.totalAmount > 0)? (this.totalAmount -= productList[productID]['price']) : (this.totalAmount = 0)
        obj['response']['data'] = productList;
        obj['response']['totalAmount'] = this.totalAmount;
        this.setState(obj);

        fetch('http://localhost:8000/products/post', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state, null, '\t')
        })
    }

    placeOrder = () => {
        let stateobj = this.state;
        let productList = this.state.response.data;

        let orderList = Object.entries(productList).reduce((orderedProducts, product) => {
            if (product[1].qty > 0) {
                orderedProducts.push({ name: product[1].name, qty: product[1].qty });
                product[1].qty = 0;
            }
            return orderedProducts;
        }, [])

        //reading orders.json
        fetch('http://localhost:8000/orders')
            .then(res => {
                return res.json()
            })
            .then(obj => {
                let orderId = Math.floor(Math.random() * 90000) + 10000;
                obj[orderId] = {
                    'orderList': orderList,
                    'totalAmount': this.state.response.totalAmount
                }
                console.log(obj);
                //writing to orders.json
                fetch('http://localhost:8000/orders/post', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(obj, null, '\t')
                })
            })
            .then(() => {
                this.totalAmount = 0;
                stateobj['response']['data'] = productList;
                stateobj['response']['totalAmount'] = this.totalAmount;
                this.setState(stateobj);

                fetch('http://localhost:8000/products/post', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(this.state, null, '\t')
                })
            })
    }

    render() {
        return (
            <ProductContext.Provider value={{
                productList: this.state.response.data,
                totalAmount: this.state.response.totalAmount,
                increment: this.increment,
                decrement: this.decrement,
                placeOrder: this.placeOrder
            }}>
                {this.props.children}
            </ProductContext.Provider>)
    }
}


export { ProductContext }
export default CartOperations