import React, { Component } from 'react'
import './App.css';
import { Routes, Route } from 'react-router-dom';
import Homepage from './Components/Homepage';
import Cart from './Components/Cart';
import CartOperations from './Components/ProductContext';
import Navbar from './Components/Navbar';
import Orders from './Components/Orders';
import ProductDetails from './Components/ProductDetails';
import OrderDetails from './Components/OrderDetails'

export class App extends Component {

  constructor(props) {
    super(props)
    this.state = {}
  }
  componentDidMount() {
    fetch('http://localhost:8000/orders')
      .then(res => {
        return res.json()
      })
      .then(obj => {
        this.setState(obj)
      })
  }

  render() {
    return (
      <div className="App">
        <CartOperations>
          <Navbar />
          <Routes>
            <Route path='/' element={<Homepage />} />
            <Route path="/:id" element={<ProductDetails />} />
            <Route path='/cart' element={<Cart />} />
            <Route path='/orders' element={<Orders />} />
            <Route path="/orders/:id" element={<OrderDetails orderList={this.state} />} />
          </Routes>
        </CartOperations>
      </div>
    )
  }
}

export default App